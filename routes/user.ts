import express from "express";
import UserController from "../controller/user";
const router = express.Router();

const userController = new UserController();

router.post("/users", userController.createNewUser);

export default router;
