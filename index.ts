import express from "express";
import userRouter from "./routes/user";
import cors from "cors";
const app = express();
const port = 3003;

app.use(cors());
app.use(express.json());
app.use(userRouter);
app.listen(port, () => console.log(`Listening on Port ${port}`));
