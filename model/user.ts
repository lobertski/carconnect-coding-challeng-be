import fs from "fs";
import path from "path";

const outputFilePath = path.resolve(__dirname, "db.json");
class UserModel {
  createUser(details: Record<string, any>) {
    const existingUsers = this.getAllUsers();
    existingUsers.push(details);
    const jsonData = JSON.stringify(existingUsers, null, 2);
    fs.writeFileSync(outputFilePath, jsonData, "utf-8");
  }

  getAllUsers() {
    try {
      const jsonData = fs.readFileSync(outputFilePath, "utf-8");
      return JSON.parse(jsonData);
    } catch (error) {
      return [];
    }
  }
}

export default UserModel;
