# Car Connect BE Challenge

This application uses Node.js, Express.js, and TypeScript. I follow the MVC architecture for the separation of concerns and maintainability of the code. The application will store data from the frontend and save it using a JSON file.