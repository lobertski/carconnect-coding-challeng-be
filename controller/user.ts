import { fields } from "./../static/fields";
import { Request, Response } from "express";
import UserModel from "../model/user";

const user = new UserModel();

class UserController {
  createNewUser(req: Request, res: Response) {
    const missingField = fields.some((field) => !req.body[field]);
    if (missingField) {
      return res.status(400).json({ message: "Bad Request" });
    }

    const newUser = {
      id: Math.floor(100000 + Math.random() * 900000),
      first_name: req.body?.first_name ?? "",
      last_name: req.body?.last_name ?? "",
      email: req.body?.email ?? "",
      phone_number: req.body?.phone_number ?? "",
      postcode: req.body?.postcode ?? "",
    };

    user.createUser(newUser);
    res.status(201).json(newUser);
  }
}

export default UserController;
